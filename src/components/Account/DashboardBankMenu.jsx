import React, { useState, Fragment } from 'react';
import { useForm, useWatch } from 'react-hook-form';
import useSound from 'use-sound';
import moneyCountingSfx from './sounds/money.mp3';
import Header from '../Header/Header';

function DashboardBankMenu() {
  const { register, errors, handleSubmit } = useForm({
    mode: 'onSubmit'
  });

  const {
    register: register2,
    errors: errors2,
    handleSubmit: handleSubmit2
  } = useForm({
    mode: 'onSubmit'
  });

  const [money, setMoney] = useState(2500);
  const [withdraw, setWithdraw] = useState(150000000);

  const [transferIsSuccess, setTransferIsSuccess] = useState(false);
  const [play] = useSound(moneyCountingSfx);

  const onMoneySubmit = (data, e) => {
    e.preventDefault();

    let inputMoney = parseInt(data.addMoney);

    if (money - inputMoney < 0) {
      return alert(`No money you have.`);
    } else {
      setMoney(money - inputMoney);
      setWithdraw(withdraw + inputMoney);
      cashOperationPlay();
    }
  }

  const onWithdrawSubmit = (data, e) => {
    e.preventDefault();

    let inputMoney = parseInt(data.withdrawMoney);

    if (withdraw - inputMoney < 0) {
      return alert(`Enough money.`);
    } else {
      setMoney(money + inputMoney);
      setWithdraw(withdraw - inputMoney);
      cashOperationPlay();
    }
  }

  const cashOperationPlay = () => {
    setTransferIsSuccess(true);
    play();
  }

  return (
    <Fragment>
      <Header
        cash={money}
        cashInBank={withdraw}
        transferIsSuccess={transferIsSuccess}
      />
      <div className="bankmnu-window">
        <div className="bankmnu-form">
          <div className="bankmnu-add">
            <form key={1} onSubmit={handleSubmit(onMoneySubmit)}>
              <label className="bankmnu-form__label" htmlFor="cash">Add cash:</label>
              <input className="bankmnu-form__input bankmnu-form__input--input" type="text"
                name="addMoney"
                ref={register({
                  required: true,
                  pattern: {
                    value: /^\d+$/i,
                    message: "Only Numbers"
                  }
                })}
              />

              <input className="bankmnu-form__btn" type="submit" value="Accept" />
            </form>

            <form key={2} onSubmit={handleSubmit2(onWithdrawSubmit)}>
              <label className="bankmnu-form__label" htmlFor="cash">Withdraw cash:</label>
              <input className="bankmnu-form__input bankmnu-form__input--input" type="text"
                name="withdrawMoney"
                ref={register2({
                  required: true,
                  pattern: {
                    value: /^\d+$/i,
                    message: "Only Numbers"
                  }
                })}
              />

              <input className="bankmnu-form__btn" type="submit" value="Withdraw" />
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default DashboardBankMenu;