import React, { Fragment } from 'react';
import AnimatedNumber from "animated-number-react";

function Balance({ isAuthenticated, cash, cashInBank, transferIsSuccess }) {

  const t = 'Cash - Withdraw';
  const [title, subtitle] = t.split('-');

  const cashWithComas = (cash) => {
    return cash.toLocaleString(navigator.language,
      {
        minimumFractionDigits: 0
      });
  }

  const CashRender = () => {
    return (
      <Fragment>
        {cashWithComas(cash)}
      </Fragment>
    );
  }

  return (
    <Fragment>
      <div className="account-header__balance">
        {
          isAuthenticated ? (
            <Fragment>
              <span className="account-header__balance--cash">{title}</span>
              <span className="account-header__balance--inbank">{subtitle}</span>
            </Fragment>
          ) : (
              <Fragment>
                <span className="account-header__balance--cash">
                  {
                    transferIsSuccess ? (
                      <AnimatedNumber
                        value={cash}
                        formatValue={cashWithComas}
                        duration={1600}
                      />
                    ) : (
                      <CashRender />
                      )
                  }
                  &#160;&euro;</span>
                <span className="account-header__balance--inbank">
                  {cashWithComas(cashInBank)} &euro;</span>
              </Fragment>
            )
        }
      </div>
    </Fragment>
  );
}

export default Balance;