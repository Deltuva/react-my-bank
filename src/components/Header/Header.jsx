import React, { Fragment } from 'react';
import Balance from '../../components/Account/Balance';

function Header({ isAuthenticated, cash, cashInBank, transferIsSuccess }) {

  return (
    <Fragment>
      <header className="account-header">
        <div className="account-header__logo">
          <img src="./ATM-sign.png" alt="ATM" />
        </div>
        <Balance
          isAuthenticated={isAuthenticated}
          cash={cash}
          cashInBank={cashInBank}
          transferIsSuccess={transferIsSuccess}
        />
      </header>
    </Fragment>
  );
}

export default Header;