import React, { useState, Fragment } from 'react';
import Header from './components/Header/Header';
import DashboardBankMenu from './components/Account/DashboardBankMenu';
import './App.sass';

function App() {
  const [pincode, setPincode] = useState("");
  const [isAuthenticated, setisAuthenticated] = useState(false);
  const [isAccountAuthenticatedCounter, setAccountAuthenticatedCounter] = useState(0);
  const [isDisabled, setIsDisabled] = useState(false);

  const handleEnter = (e) => {
    e.preventDefault();

    const maxAuthenticateds = 3;
    const pincodeRegex = /(?=.*[0-9])/;
    const currentPincode = '1234';

    if (!pincode || pincode.length < 4 || !pincodeRegex.test(pincode)) {
      alert(`Invalid PIN code.`);
    } else if (pincode === currentPincode) {
      console.log(`Enter Success`);

      setAccountAuthenticatedCounter(0);
      setisAuthenticated(true);

    } else {

      if (isAccountAuthenticatedCounter >= maxAuthenticateds)
        return setIsDisabled(true)

      setAccountAuthenticatedCounter(isAccountAuthenticatedCounter + 1);
    }
  }

  const onSetPincode = (code) => {
    setPincode(code);
  };

  return (
    <Fragment>
      {
        !isAuthenticated ? (
          <Header
            isAuthenticated={!isAuthenticated}
          />
        ) : (
            <span></span>
          )

      }
      <div className="container">
        {
          !isAuthenticated ? (
            <div className="bankmnu-window bankmnu--window">
              <div className="bankmnu-form">
                <form onSubmit={handleEnter}>
                  <label className="bankmnu-form__label" htmlFor="pin">Enter PIN code:</label>
                  <input className="bankmnu-form__input bankmnu-form__input--input"
                    type="password"
                    value={pincode}
                    onChange={e => onSetPincode(e.target.value)}
                    placeholder="*****" />
                  <button className="bankmnu-form__btn" disabled={isDisabled}>Enter</button>
                </form>
              </div>
            </div>
          ) : (
              <DashboardBankMenu />
            )
        }
      </div>
    </Fragment>
  );
}

export default App;
